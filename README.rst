***
CAD
***

.. image:: https://readthedocs.org/projects/frnsroycad/badge/?version=latest
  :target: https://frnsroycad.readthedocs.io/en/latest/?badge=latest
  :alt: Documentation Status

.. image:: https://circleci.com/bb/frnsroy/cad.svg?style=svg
  :target: https://circleci.com/bb/frnsroy/workflows/cad
  :alt: Continuous Integration Status

.. image:: https://codecov.io/bb/frnsroy/cad/branch/master/graph/badge.svg
  :target: https://codecov.io/bb/frnsroy/cad
  :alt: Code Coverage Status