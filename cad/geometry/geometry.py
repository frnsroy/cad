# -*- coding: utf-8 -*-
"""Generate a predefined geometry or import it from a valid CAD file.

:Author: **Francois Roy**
:Date: |today|

.. moduleauthor:: Francois Roy <frns.roy@gmail.com>
"""


class Geometry(object):
    r""""""
    def __init__(self):
        super().__init__()

    def run(self):
        r""""""
        return 0


if __name__ == '__main__':
    geometry()
