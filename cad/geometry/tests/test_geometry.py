# -*- coding: utf-8 -*-
"""Test the geometry module.

:Author: **Francois Roy**
:Date: |today|

.. moduleauthor:: Francois Roy <frns.roy@gmail.com>
"""
from cad.geometry.geometry import Geometry


def test_run():
    g = Geometry()
    assert g.run() == 0
